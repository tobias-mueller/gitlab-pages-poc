# GitLab Pages auf Open CoDE

Dies ist ein Beispielrepository, welches den Einsatz von GitLab Pages durch die CI/CD Pipeline an einem einfachen Beispiel illustriert.

Das Beispielprojekt nutzt den Statischen Website Generator Jekyll und ein einfaches HTML Dokument. 
Es sind jegliche Statische Website Generators / Frameworks oder plain HTML in GitLab Pages einsetzbar.

Eine beispielhafte Liste der Statischen Website Generators und weitere Dokumentation können Sie der [GitLab Dokumentation](https://docs.gitlab.com/ee/user/project/pages/) entnehmen. 
